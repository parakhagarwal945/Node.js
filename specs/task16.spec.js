const chai = require("chai");
const expect = chai.expect;
const fstasks = require("../src/task16");
const fs = require("fs");
const syncCheck = require("./syncCheck.spec");

describe("task 16 test running", () => {
  it("should ensure whether file exists or not", done => {
    let pathToFile = "./src/contents/File1.md";
    let content = "Content provided by user.";
    fstasks(pathToFile, content).catch(error => {
      expect(error).to.equal("File already exists provide another name");
      done();
    });
  });
  it("should not contain synchronous function",done => {
    syncCheck("./src/task16.js").then(res => {
      expect(res).to.equal(true);
      done();
    });
  });
  it("should create a file and write the content in the file", done => {
    let pathToFile = "./src/contents/innerContent/konfinityMember.txt";
    let content = "This file contains data of student";
    fstasks(pathToFile, content).then(response => {
      expect(response).to.equal("Content written to file successfully");
      fstasks("./src/contents/File1.md", "This is second file in md format").then(res=>{})
      done();
    });
  });
  it("file should be created and contain the content", done => {
    let pathToFile = "./src/contents/innerContent/konfinityMember.txt";
    checkFile(pathToFile).then(response => {
      expect(response).to.equal("File exists");
      done();
    });
  });
  it("File should contain the data provided #end_test", done => {
    let pathToFile = "./src/contents/innerContent/konfinityMember.txt";
    fs.readFile(pathToFile, "utf8", (error, data) => {
      if (!error) {
        expect(data).to.include("This file contains data of student");
        removeFile("./src/contents/innerContent/konfinityMember.txt").then(
          result => {
            console.log("Task completed");
          }
        );
        done();
      }
    });
  });
});

const checkFile = filename => {
  return new Promise((resolve, reject) => {
    fs.access(filename, error => {
      if (error) {
        reject("File does not exists");
      } else {
        resolve("File exists");
      }
    });
  });
};

const removeFile = filename => {
  return new Promise((resolve, reject) => {
    fs.unlink(filename, error => {
      if (!error) {
        resolve("File removed");
      } else {
        reject("Error occured");
      }
    });
  });
};
