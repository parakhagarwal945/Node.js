const chai = require("chai");
const expect = chai.expect;
const fstask = require("../src/task11");
const syncCheck = require("./syncCheck.spec");

describe("task 11 test running", () => {
  it("should return content of file if exists or return error", done => {
    let pathToFile = "./src/contents/File1.md";
    fstask(pathToFile).then(response => {
      expect(response).to.be.a("string");
      done();
    });
  });
  it("should not contain synchronous function",done => {
    syncCheck("./src/task11.js").then(res => {
      expect(res).to.equal(true);
      done();
    });
  });
  it("should return content of file if exists or return error", done => {
    let pathToFile = "./src/contents/File1.md";
    fstask(pathToFile).then(response => {
      expect(response).to.include("This is second file in md format");
      done();
    });
  });
  it("should return content of file if exists or return error #end_test", done => {
    let pathToFile = "./src/conts/File1.md";
    fstask(pathToFile).catch(error => {
      expect(error).to.equal("File does not exists");
      done();
    });
  });
});
