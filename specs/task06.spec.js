const chai = require("chai");
const expect = chai.expect;
const fstasks = require("../src/task06");
const syncCheck = require("./syncCheck.spec");
const fs = require("fs");

describe("task 06 test running", () => {
  it("should return the count of js files", done => {
    let pathToDir = "./src/contents/";
    fstasks(pathToDir).then(userResult => {
      expect(userResult.countJs).to.equal(6);
      done();
    });
  });
  it("should return the count of txt files", done => {
    let pathToDir = "./src/contents/";
    fstasks(pathToDir).then(userResult => {
      expect(userResult.countTxt).to.equal(2);
      done();
    });
  });
  it("should not contain synchronous function",done => {
    syncCheck("./src/task06.js").then(res => {
    expect(res).to.equal(true);
    done();
  });
});
  it("should return the count of remaining files", done => {
    let pathToDir = "./src/contents/";
    fstasks(pathToDir).then(userResult => {
      expect(userResult.count).to.equal(5);
      done();
    });
  });
  it("should return the count of js files #end_test", done => {
    let pathToDir = "./jshdjas/";
    fstasks(pathToDir).catch(userResult => {
      expect(userResult).to.equal("Error occured while reading directory");
      done();
    });
  });
});
