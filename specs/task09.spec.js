const chai = require("chai");
const expect = chai.expect;
const fstasks = require("../src/task09");
const syncCheck = require("./syncCheck.spec");

describe("task 09 test running", () => {
  it("should return the count of Files at given path", done => {
    let pathToDir = "./src/contents/";
    fstasks(pathToDir).then(count => {
      expect(count.countFile).to.equal(26);
      done();
    });
  });
  it("should not contain synchronous function",done => {
    syncCheck("./src/task09.js").then(res => {
      expect(res).to.equal(true);
      done();
    });
  });
  it("should return the count of Directory at given path", done => {
    let pathToDir = "./src/contents/";
    fstasks(pathToDir).then(count => {
      expect(count.countDir).to.equal(3);
      done();
    });
  });
  it("should return the count of Files at given path", done => {
    let pathToDir = "./src/contents/innerContent";
    fstasks(pathToDir).then(count => {
      expect(count.countFile).to.equal(10);
      done();
    });
  });
  it("should return the count of Directory at given path #end_test", done => {
    let pathToDir = "./src/contents/innerContent";
    fstasks(pathToDir).then(count => {
      expect(count.countDir).to.equal(1);
      done();
    });
  });
});
