const chai = require("chai");
const expect = chai.expect;
const fstask = require("../src/task12");
const syncCheck = require("./syncCheck.spec");

describe("task 12 test running", () => {
  it("should return content of file from a directory if exists or return error", done => {
    let pathToFile = "./src/contents/";
    let index = 3;
    fstask(pathToFile, index).then(response => {
      expect(response.data).to.be.a("string");
      done();
    });
  });
  it("should not contain synchronous function",done => {
    syncCheck("./src/task12.js").then(res => {
      expect(res).to.equal(true);
      done();
    });
  });
  it("should return content of file from a directory if exists or return error", done => {
    let pathToFile = "./src/contents/";
    let index = 3;
    fstask(pathToFile, index).then(response => {
      expect(response.data).to.include(
        "This is File 4 for FS Module in txt format"
      );
      done();
    });
  });
  it("should return name of file from a directory if exists or return error", done => {
    let pathToFile = "./src/contents/";
    let index = 3;
    fstask(pathToFile, index).then(response => {
      expect(response.filename).to.equal("File4.txt");
      done();
    });
  });
  it("should return content of file if exists or return error #end_test", done => {
    let pathToFile = "./src/conts/";
    fstask(pathToFile, 3).catch(error => {
      expect(error).to.include(
        "Error occured while fetching files from Directory"
      );
      done();
    });
  });
});
