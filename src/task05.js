const path=require("path")
const fs=require("fs")
const a=(dirpath)=>{
    return new Promise((res,rej)=>{
        fs.readdir(dirpath,(err,files)=>{
            if(err)   rej("Error occured while reading directory")
            else res(files);
        })
    })
}
module.exports=a;