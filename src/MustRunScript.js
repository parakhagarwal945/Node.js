const fs = require("fs");
const path = require("path");

const ensureFile = pathToFile => {
  return new Promise((resolve, reject) => {
    fs.access(pathToFile, error => {
      if (error) {
        resolve("File Does not exists");
      } else {
        reject("File Exists");
      }
    });
  });
};

const makeDir = async (pathToDir, dirName) => {
  let completePath = path.join(pathToDir, dirName);
  ensureFile(completePath)
    .then(response => {
      if (response === "File Does not exists") {
        fs.mkdir(completePath, error => {
          if (error) {
            console.log("Error occured while creating File ");
          } else {
            console.log(dirName, " File Created");
          }
        });
      }
    })
    .catch(error => {
      console.log(error);
    });
};

const setDataToFile = async (pathToFile, filename, content) => {
  let fileName = path.join(pathToFile, filename);
  ensureFile(fileName)
    .then(response => {
      if (response === "File Does not exists") {
        fs.writeFile(fileName, content, error => {
          if (error) {
            console.log("Error occured while writing content to file");
          } else {
            console.log("Content written to file sucessfully");
          }
        });
      }
    })
    .catch(error => {
      console.log(error);
    });
};

const mainFunction = async () => {
  try {
    await makeDir(__dirname, "contents");
    await makeDir(__dirname, "files");
    let contentPath = path.join(__dirname, "contents");
    let filesPath = path.join(__dirname, "files");
    await makeDir(contentPath, "innerContent");
    let innerContentPath = path.join(contentPath, "innerContent");
    await makeDir(innerContentPath, "nestedFolder");
    let nestedFolderPath = path.join(innerContentPath, "nestedFolder");
    await makeDir(contentPath, "inContentFolder");
    let inContentFolderPath = path.join(contentPath, "inContentFolder");

    await setDataToFile(nestedFolderPath, "File1.txt", "File1 inside nested");
    await setDataToFile(nestedFolderPath, "File2.txt", "File2 inside nested");
    await setDataToFile(nestedFolderPath, "File3.txt", "File3 inside nested");
    await setDataToFile(nestedFolderPath, "File4.txt", "File4 inside nested");
    await setDataToFile(nestedFolderPath, "File5.txt", "File5 inside nested");

    await setDataToFile(
      inContentFolderPath,
      "File1.txt",
      "File1 inside inContent"
    );

    await setDataToFile(
      inContentFolderPath,
      "File2.txt",
      "File2 inside inContent"
    );
    await setDataToFile(
      inContentFolderPath,
      "File3.txt",
      "File3 inside inContent"
    );
    await setDataToFile(
      inContentFolderPath,
      "File4.txt",
      "File4 inside inContent"
    );
    await setDataToFile(
      inContentFolderPath,
      "File5.txt",
      "File5 inside inContent"
    );

    await setDataToFile(
      contentPath,
      "1.Introduction.md",
      "This is an introduction file for FS Module"
    );
    await setDataToFile(
      contentPath,
      "File1.md",
      "This is second file in md format"
    );
    await setDataToFile(
      contentPath,
      "File3.txt",
      "This is File 3 in txt format"
    );
    await setDataToFile(
      contentPath,
      "File4.txt",
      "This is File 4 for FS Module in txt format"
    );
    await setDataToFile(
      contentPath,
      "File5.md",
      "This is File 5 for FS Module in md format"
    );
    let data =
      "const konfinity = () => { \n var a = 30; \n let b = 10; \n const c = a+b; \n return c; \n }; \n function display(){ \n let res = konfinity(); \n console.log(res); \n }";
    await setDataToFile(contentPath, "fs000.js", data);
    let dataForJs =
      'const konfinity = () => { \n var a = "Mayank"; \n let b = "Banga"; \n const c = "Konfinity"; \n }';
    await setDataToFile(contentPath, "fs001.js", dataForJs);
    await setDataToFile(
      contentPath,
      "fs002.js",
      " // This is second js file for FS Module"
    );
    await setDataToFile(
      contentPath,
      "fs003.js",
      "// This is  third js file for FS Module"
    );
    await setDataToFile(
      contentPath,
      "fs004.js",
      "// This is fourth js file for FS Module"
    );
    await setDataToFile(
      contentPath,
      "fs005.js",
      "// This is fifth js file for FS Module"
    );
    await setDataToFile(filesPath, "ex1.exe", "File 1 inside files folder");
    await setDataToFile(
      filesPath,
      "ex2.txt",
      "File 2 inside files folder. Happy Coding ! "
    );
    await setDataToFile(
      filesPath,
      "Konfinity.txt",
      "This is konfinity infotech."
    );
    await setDataToFile(
      filesPath,
      "KonfinityMoto.txt",
      "Learn to code, talk is cheap."
    );
    await setDataToFile(
      innerContentPath,
      "Mayank.txt",
      "This contains data of Konfinity Member. Name : Mayank Banga"
    );
    await setDataToFile(
      innerContentPath,
      "Aayush.txt",
      "This contains data of Konfinity Member. Name : Aayush Agarwal"
    );
    await setDataToFile(
      innerContentPath,
      "Anubhav.txt",
      "This contains data of Konfinity Member. Name : Anubhav"
    );
    await setDataToFile(
      innerContentPath,
      "Vaishnavi.txt",
      "This contains data of Konfinity Member. Name : Vaishnavi Narang"
    );
    await setDataToFile(
      innerContentPath,
      "Kamal.txt",
      "This contains data of Konfinity Member. Name : Kamal Pandey"
    );
  } catch (error) {
    console.log(error);
  }
};

mainFunction();
