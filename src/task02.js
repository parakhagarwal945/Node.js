const path=require("path")
const fs=require("fs")
const task02=(filename)=>{
return new Promise((res,rej)=>{
    fs.stat(filename,(err,stats)=>{
        if(err)  rej("Error occured while calculating stats")
else res(stats.size);
    })
})
}
module.exports=task02;