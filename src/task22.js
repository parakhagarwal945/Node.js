const path=require("path")
const fs=require("fs")
const task04=require("./task04.js");
const a=(dirpath)=>{
    return new Promise((res,rej)=>{
task04(dirpath)
.then(msg=>{
    fs.rmdir(dirpath,(err)=>{
        res("File exists : Directory removed")
    })
})
.catch(msg=>{
    rej("cannot remove directory because : File does not exists")
})
    })
}
module.exports=a;