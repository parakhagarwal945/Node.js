const path=require("path")
const fs=require("fs")
const task04=require("./task04.js")
const task21=require("./task21.js")
const task22=require("./task22.js")

const a=(dirpath)=>{
return new Promise((res,rej)=>{
    task04(dirpath)
    .then(msg=>{
        fs.rmdir(dirpath,(err)=>{
            if(err)  rej("cannot remove directory because : File does not exists")
         else   res("File exists : Directory removed")
        })
    })
    task21(dirpath)
    task22(dirpath)
    .catch(msg=>{
fs.writeFile(dirpath,(err)=>{
    if(err)  rej("cannot remove directory because : File does not exists")
    else {
        fs.rmdir(dirpath,(err)=>{
            if(err)  rej("cannot remove (directory because : File does not exists")
            else   res("File exists : Directory removed")
        })
    }
})
})
    })
}
module.exports=a;