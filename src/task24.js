const path=require("path")
const fs=require("fs")
const a=(filepath)=>{
    return new Promise((res,rej)=>{
        fs.unlink(filepath,(err)=>{
            if(err)  rej("error deleting file")
            else res( "File deleted successfully")
        })
    })
}
module.exports=a;