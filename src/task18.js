const path = require("path")
const fs = require("fs")
const a = (sf,df) => {
    return new Promise((res, rej) => {
        fs.access(sf, (err) => {
            if (!err) {
                fs.readFile(sf,"utf-8",(err,content)=>{
                    if(err)  rej("Source File does not exists")
                    else{
                        fs.access(df,(err)=>{
                            if(!err)  
                             {
                                fs.readFile(df,"utf-8",(err,content1)=>{
                                    if(err)  res(content1)
                                    else {
                                      fs.writeFile(df,content+" "+content1,(err)=>{
                                          if(err)  rej("Source File does not exists")
                                          else{
                                              res( "Content written to file successfully")
                                          }
                                      })
                                    }
                                })
                            }
                            else{
                                fs.writeFile(df,content,(err)=>{
                                    if(err)  rej("Source File does not exists")
                                   else res( "Content written to file successfully")
                                    
                                })
                            }
                        })
                    }
                })
            }
            else {
                rej("Source File does not exists")
            }
        })
    })
}

module.exports=a;