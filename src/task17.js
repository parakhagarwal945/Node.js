const path=require("path")
const fs=require("fs")
const a=(filepath,data)=>{
    return new Promise((res,rej)=>{
        fs.access(filepath,(err)=>{
            if(!err){
fs.readFile(filepath,"utf-8",(err,content)=>{
    if(err)  rej("File does not exists provide another name")
    else{
        fs.writeFile(filepath,content+" "+data,(err)=>{
            if(err)  rej("File does not exists provide another name")
            else res("Content appended to file successfully")
        })
    }
})
            }
            else rej("File does not exists provide another name")
        })
    })
}
module.exports=a;