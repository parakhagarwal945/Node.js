const task04 = require("./task04");
const fs = require("fs");
const path=require("path");
const a=(filepath,content)=>{
    return new Promise((res,rej)=>{
        task04(filepath)
    .then(msg=>{
        fs.appendFile(filepath,content,(err)=>{
             res("Data appended successfully")
        })
    }) 
    .catch(msg=>{
      rej("Cannot append data because : File does not exists")
    })
    })
}
module.exports=a;
