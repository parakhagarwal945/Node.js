
const fs = require("fs");
const path=require("path");
const task04 = require("./task04");
const a=(sf,df)=>{
return new Promise((res,rej)=>{
    task04(df)
    .then(msg=>{
        rej("cannot copy data : File exists")
    })
    .catch(msg=>{
        fs.copyFile(sf,df,(error)=>{
            res("Content copied successfully")
        })
    })
})
}
module.exports=a;
