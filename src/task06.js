const path = require("path")
const fs = require("fs")
const a = (dirpath) => {
    return new Promise((res, rej) => {
        fs.readdir(dirpath, (err, files) => {
            if (err) rej("Error occured while reading directory")
            else {
                const x = get(files)
                res(x)
            }
        })
    })
}
function get(files) {
    let countJs = 0;
    let countTxt = 0;
     let count = 0;
     console.log("total file length is",files.length)
    for (var i = 0; i < files.length; i++) {
        var b = files[i].split(".")
        if (b[1] === "js") {
            countJs += 1
            console.log("countjs is",countJs);
        }
        else if (b[1] === "txt") {
            countTxt += 1;
            console.log("counttxt",countTxt);
        }
     else {
         count += 1;
         console.log("count is",count);
     }
        var obj={
            countJs:countJs,
            countTxt:countTxt,
            count:count,
            files:files,
        }
        // return obj;
    }
    return obj;
}
module.exports = a;
