const path=require("path")
const fs=require("fs")
const a=(dirpath)=>{
    return new Promise((res,rej)=>{
        fs.mkdir(dirpath,(err)=>{
            if(err)  rej( "Error occured while making directory")
            else res("New directory created")
        })
    })
}
a()
.then(msg=>{
    console.log(msg)
})
.catch(msg=>{
    console.log(msg)
})
module.exports=a;