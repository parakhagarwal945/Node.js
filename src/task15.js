
const path=require("path")
const fs=require("fs")
const a=(filepath,data)=>{
    return new Promise((res,rej)=>{
        fs.writeFile(filepath,data,(err)=>{
            if(err)  rej("Error writing content in file")
            else res("Content written to file successfully")
        })
    })
}
module.exports=a;
