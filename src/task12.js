const path=require("path")
const fs=require("fs")
const a=(dirpath,index)=>{
    return new Promise((res,rej)=>{
        fs.readdir(dirpath,(err,files)=>{
            if(err)  rej("Error occured while fetching files from Directory")
            else{
                fs.readFile(path.join(dirpath,files[index]),"utf-8",(err,data)=>{
                    if(err)  rej("Error occured while fetching files from Directory")
                    else res({
                        data:data,
                        filename:files[index]
                    })
                })
            }
        })
    })
}
module.exports=a;