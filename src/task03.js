const path=require("path")
const fs=require("fs")
const task03=(filepath)=>{
    return new Promise((res,rej)=>{
        fs.stat(filepath,(err,stats)=>{
            if(err)  rej("Error occured while calculating stats")
            else res({
birthTime:stats.birthtimeMs,
modifiedTime:stats.mtimeMs,
size:stats.size,
type:stats.isFile()?"File":"Directory",
            })
        })
    })
}
module.exports=task03;